/*
 * sound.c
 *
 *  Created on: May 15, 2018
 *      Author: rsv
 */

#include "stm32f0xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"

extern TIM_HandleTypeDef htim17;

#define COMPARE 10

void soundTask( void * pvParam )
{

	//TIM_SetAutoreload( TIM17, per );
	//TIM_SetCompare1( TIM17, COMPARE );

	__HAL_TIM_SET_AUTORELOAD( &htim17, 1000 );
	__HAL_TIM_SET_COMPARE( &htim17, TIM_CHANNEL_1, COMPARE );
	__HAL_TIM_SET_COUNTER( &htim17, 0 );
	//HAL_TIM_Base_Start( &htim17 );
	//HAL_TIM_OC_Start( &htim17, TIM_CHANNEL_1 );
	//HAL_TIM_PWM_Start( &htim17, TIM_CHANNEL_1 );
	//HAL_TIMEx_PWMN_Start(&htim17, TIM_CHANNEL_1);

	while (1) {
		HAL_TIMEx_PWMN_Start(&htim17, TIM_CHANNEL_1);
		vTaskDelay( pdMS_TO_TICKS(100) );
		HAL_TIMEx_PWMN_Stop(&htim17, TIM_CHANNEL_1);
		vTaskDelay( pdMS_TO_TICKS(1900) );

		/*HAL_GPIO_WritePin(dbg_GPIO_Port, dbg_Pin, GPIO_PIN_RESET);
		vTaskDelay( pdMS_TO_TICKS(500) );
		HAL_GPIO_WritePin(dbg_GPIO_Port, dbg_Pin, GPIO_PIN_SET);*/
	}
}

