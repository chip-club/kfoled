/*
 * oled.c
 *
 *  Created on: May 27, 2018
 */

#include "stm32f0xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"

#include "main.h"
#include "oled.h"
#include "image.h"
#include "font.h"
#include "menu.h"

#include <string.h>
#include <stdarg.h>
#include <stdio.h>


extern SPI_HandleTypeDef hspi1;

// Screen dimensions
#define SCR_W	(uint8_t)128 // width
#define SCR_H	(uint8_t)64  // height
#define PAGES	SCR_H / 8    // num pages

// Video RAM buffer
static uint8_t crd[SCR_W * PAGES]; // __attribute__((aligned(4)));
static uint8_t Xcoor = 0; // current X coordinate
static uint8_t Page = 0;  // current X coordinate
static uint8_t Yofs  = 0; // offset
static font_e font = f_5x8;

#define SH1106_CS_H()  HAL_GPIO_WritePin(lcd_ce_GPIO_Port, lcd_ce_Pin, GPIO_PIN_SET)
#define SH1106_CS_L()  HAL_GPIO_WritePin(lcd_ce_GPIO_Port, lcd_ce_Pin, GPIO_PIN_RESET)
#define SH1106_RST_H() HAL_GPIO_WritePin(lcd_rst_GPIO_Port, lcd_rst_Pin, GPIO_PIN_SET)
#define SH1106_RST_L() HAL_GPIO_WritePin(lcd_rst_GPIO_Port, lcd_rst_Pin, GPIO_PIN_RESET)
#define SH1106_DC_H()  HAL_GPIO_WritePin(lcd_dc_GPIO_Port, lcd_dc_Pin, GPIO_PIN_SET)
#define SH1106_DC_L()  HAL_GPIO_WritePin(lcd_dc_GPIO_Port, lcd_dc_Pin, GPIO_PIN_RESET)

// SH1106 command definitions
#define SH1106_CMD_SETMUX    (uint8_t)0xA8 // Set multiplex ratio (N, number of lines active on display)
#define SH1106_CMD_SETOFFS   (uint8_t)0xD3 // Set display offset
#define SH1106_CMD_STARTLINE (uint8_t)0x40 // Set display start line
#define SH1106_CMD_SEG_NORM  (uint8_t)0xA0 // Column 0 is mapped to SEG0 (X coordinate normal)
#define SH1106_CMD_SEG_INV   (uint8_t)0xA1 // Column 127 is mapped to SEG0 (X coordinate inverted)
#define SH1106_CMD_COM_NORM  (uint8_t)0xC0 // Scan from COM0 to COM[N-1] (N - mux ratio, Y coordinate normal)
#define SH1106_CMD_COM_INV   (uint8_t)0xC8 // Scan from COM[N-1] to COM0 (N - mux ratio, Y coordinate inverted)
#define SH1106_CMD_COM_HW    (uint8_t)0xDA // Set COM pins hardware configuration
#define SH1106_CMD_CONTRAST  (uint8_t)0x81 // Contrast control
#define SH1106_CMD_EDON      (uint8_t)0xA5 // Entire display ON enabled (all pixels on, RAM content ignored)
#define SH1106_CMD_EDOFF     (uint8_t)0xA4 // Entire display ON disabled (output follows RAM content)
#define SH1106_CMD_INV_OFF   (uint8_t)0xA6 // Entire display inversion OFF (normal display)
#define SH1106_CMD_INV_ON    (uint8_t)0xA7 // Entire display inversion ON (all pixels inverted)
#define SH1106_CMD_CLOCKDIV  (uint8_t)0xD5 // Set display clock divide ratio/oscillator frequency
#define SH1106_CMD_DISP_ON   (uint8_t)0xAF // Display ON
#define SH1106_CMD_DISP_OFF  (uint8_t)0xAE // Display OFF (sleep mode)

#define SH1106_CMD_COL_LOW   (uint8_t)0x00 // Set Lower Column Address
#define SH1106_CMD_COL_HIGH  (uint8_t)0x10 // Set Higher Column Address
#define SH1106_CMD_PAGE_ADDR (uint8_t)0xB0 // Set Page Address

#define SH1106_CMD_VCOMH     (uint8_t)0xDB // Set Vcomh deselect level
#define SH1106_CMD_SCRL_HR   (uint8_t)0x26 // Setup continuous horizontal scroll right
#define SH1106_CMD_SCRL_HL   (uint8_t)0x27 // Setup continuous horizontal scroll left
#define SH1106_CMD_SCRL_VHR  (uint8_t)0x29 // Setup continuous vertical and horizontal scroll right
#define SH1106_CMD_SCRL_VHL  (uint8_t)0x2A // Setup continuous vertical and horizontal scroll left
#define SH1106_CMD_SCRL_STOP (uint8_t)0x2E // Deactivate scroll
#define SH1106_CMD_SCRL_ACT  (uint8_t)0x2F // Activate scroll

static void SH1106_cmd(uint8_t cmd) {
	SH1106_CS_L();
	SH1106_DC_L();
	uint8_t command = cmd;
	HAL_SPI_Transmit ( &hspi1,  &command, 1, 80) ;
	SH1106_CS_H();
}

static void SH1106_cmd_double(uint8_t cmd1, uint8_t cmd2) {
	SH1106_CS_L();
	SH1106_DC_L();
	uint8_t command[2];
	command[0] = cmd1;
	command[1] = cmd2;
	HAL_SPI_Transmit ( &hspi1,  command, 2, 80) ;
	SH1106_CS_H();
}

void SH1106_Init(void) {
	// Hardware display reset
	SH1106_CS_H();
	SH1106_RST_L();
	vTaskDelay( pdMS_TO_TICKS(50) );
	SH1106_RST_H();
	SH1106_cmd_double(SH1106_CMD_SETMUX,0x3F); // 64MUX
	SH1106_cmd_double(SH1106_CMD_SETOFFS,0x00); // Offset: 0
	SH1106_cmd(SH1106_CMD_STARTLINE | 0x00); // Start line: 0
	SH1106_cmd(SH1106_CMD_SEG_INV);// segment re-map (X coordinate)
	SH1106_cmd(SH1106_CMD_COM_INV);// COM output scan direction (Y coordinate)
	SH1106_cmd_double(SH1106_CMD_COM_HW,0x12); // Set COM pins hardware configuration
	// Set Vcomh deselect level, values: 0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70
	// This value affects on contrast level
	SH1106_cmd_double(SH1106_CMD_VCOMH,0x35); // ~0.83V x Vcc
	SH1106_cmd_double(SH1106_CMD_CONTRAST,0x80); // Contrast: middle level
	// Disable entire display ON
	SH1106_cmd(SH1106_CMD_EDOFF); // Display follows RAM content
	SH1106_cmd(SH1106_CMD_INV_OFF); // Normal display mode
	// 0xF0 value gives maximum frequency (maximum Fosc without divider)
	// 0x0F value gives minimum frequency (minimum Fosc divided by 16)
	SH1106_cmd_double(SH1106_CMD_CLOCKDIV,0x40); // !!!
	// Display ON
	SH1106_cmd(SH1106_CMD_DISP_ON); // Display enabled
	// Assert CS pin
	SH1106_CS_H();
}

// Send vRAM buffer into display
void SH1106_Flush(void) {
	for (register uint32_t page = 0; page < PAGES; ++page) {
		SH1106_cmd(SH1106_CMD_PAGE_ADDR | page);
		SH1106_cmd(SH1106_CMD_COL_LOW | 2);
		SH1106_cmd(SH1106_CMD_COL_HIGH);
		SH1106_DC_H();
		uint8_t* vram = &crd[page * SCR_W];
		SH1106_CS_L();
		HAL_SPI_Transmit(&hspi1, vram, SCR_W, 1000);
		SH1106_CS_H();
	}
}

//88888888888888888888888888888888888888888888888888888888888888888888888888

/*
 * getUCode() получает код ucs16 из строки формата UTF-8
 * Здесь в упрощенной функции, код unicode ограничен 2-я байтами,
 * а соответсвующий код UTF-8 может занимать от 1 до 3х байт.
 * Params:
 *   *str - указатель на строку
 *   *code - куда запишется результат - код юникод
 * Return:
 *   от 0 до 3 - сколько байт занял символ (чтобы вызывающий код
 *   в дальнейшем передвинул указатель строки на столько байт)
 */
int getUCode( const char* str, uint16_t *code ) {
	uint32_t abc = 0;

	if ( *str != 0) { // if not NULL symbol
		if ( (*str & 0x80) == 0) { // if 1 byte
			*code = *str;
			return 1;
		}
		if ( (*str & 0xE0) == 0xC0 ) { // if 2 bytes 0b110..
			abc = *str;
			str++;
			if ( (*str & 0xC0) != 0x80 ) { // error no 0b10.. bits
				*code = 0;
				return 1;
			}
			abc <<= 8;
			abc |= *str;

			*code = abc & 0x3F;
			abc >>= 2;
			*code |= abc & 0x07C0;
			return 2;
		}
		if ( (*str & 0xF0) == 0xE0 ) { // if 3 bytes 0b1110..
			abc = *str; // first byte
			abc <<= 8;
			str++;
			if ( (*str & 0xC0) != 0x80 ) { // error no 0b10.. bits
				*code = 0;
				//urtPrint("utf8 err1\n");
				return 1;
			}
			abc |= *str; // second byte
			abc <<= 8;
			str++;
			if ( (*str & 0xC0) != 0x80 ) { // error no 0b10.. bits
				*code = 0;
				//urtPrint("utf8 err2\n");
				return 2;
			}
			abc |= *str; // third byte

			*code = abc & 0x3F;
			abc >>= 2;
			*code |= abc & 0x0FC0;
			abc >>= 2;
			*code |= abc & 0xF000;
			//urtPrint("get code: ");
			//urt_uint16_to_5str( *code );
			//urtPrint("\n");
			return 3;
		}
	}
	*code = 0;
	return 0;
}

void disClear(void) {
	memset( crd, 0, SCR_W * PAGES);
}

int disSet( uint8_t X, uint8_t Y )
{
	//if ( Y > SCR_H ) { return -1; }
	//if ( X > SCR_W ) { return -1; }
	Page  = Y >> 3;
	Yofs  = Y % 8;
	Xcoor = X;
	return 0;
}

void disSetF( uint8_t X, uint8_t Y, font_e fnt )
{
	if ( disSet( X, Y ) != 0 ) { return; }
	font = fnt;
}

void disDrawImg(int iwidth, int ipages, const char* img)
{
	uint16_t hw;
	int page = Page;
	int xcoor = Xcoor;
	int picpage = 0;

	while ( ipages > 0 ) {
		xcoor = Xcoor;
		if ( page  >= PAGES ) { break; }
		int flag = (Yofs > 0)  &  ( (page + 1) < PAGES );
		for (int dx = 0; dx < iwidth; dx++) {
			hw = (uint16_t)img[picpage * iwidth + dx] << Yofs;
			crd[page * SCR_W + xcoor] |= (char)hw;
			if ( flag ) {
				crd[(page+1) * SCR_W + xcoor] |= (hw >> 8);
			}
			xcoor++;
			if ( xcoor >= SCR_W ) { break; }
		}
		ipages--;
		page++;
		picpage++;
	}
	Xcoor = xcoor;
}

void disPuts( const char* str )
{
	uint16_t code;

	while (1) {
		if ( Xcoor >= SCR_W ) { break; }
		if ( Page  >= PAGES ) { break; }

		str += getUCode( str, &code );
		if ( code == 0 ) { // если конец строки
			break;
		}
		Symbol_t sym;
		switch ( font ) {
			case f_5x8:
				sym = getSymImg(&font5x8, code);
				disDrawImg(sym.width, sym.pages, sym.img); // 17, 3, getFont_deja17x24( code ));
				Xcoor++; // space after character
				break;
			case f_d24x17:
				sym = getSymImg(&deja17x24, code);
				disDrawImg(sym.width, sym.pages, sym.img); // 17, 3, getFont_deja17x24( code ));
				Xcoor++; // space after character
				break;
		}
	}
}

void disPrintf( const char *format, ...)
{
	char buf[22];
	va_list args;
	va_start( args, format );
	vsnprintf(buf,22,format,args);
	va_end( args );
	disPuts(buf);
}


void oledTask( void * pvParam )
{
	vTaskDelay( pdMS_TO_TICKS(50) );
	SH1106_Init();
	//memcpy(crd, bmwcar, 1024);
	disClear();
	SH1106_Flush();
	//vTaskDelay( pdMS_TO_TICKS(10000) );
	while(1) {
		disClear();
		mTestFont(0);
		SH1106_Flush();
		vTaskDelay( pdMS_TO_TICKS(100) );
		//SH1106_cmd(SH1106_CMD_INV_ON);
		//vTaskDelay( pdMS_TO_TICKS(200) );
		//SH1106_cmd(SH1106_CMD_INV_OFF);
	}
}

