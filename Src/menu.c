/*
 * menu.c
 *
 *  Created on: Jun 11, 2018
 *      Author: rsv
 */

#include "stm32f0xx_hal.h"
#include "font.h"
#include "oled.h"
#include "menu.h"

int mTestFont(uint8_t ev) {
	static const int ofs = 0;
	static uint16_t val = 0;

	disClear();
	disSetF(27, ofs, f_5x8 );
	disPuts( "Тест дисплея" );

	disSetF(0, ofs + 24, f_d24x17 );
	disPrintf("%4u Al", val);

	//ofs++;
	//if (ofs > 63) {
	//	ofs = 0;
	//}
	val++;
	return 1;
}
