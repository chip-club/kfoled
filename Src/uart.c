/*
 * uart.c
 *
 *  Created on: May 24, 2018
 *      Author: rsv
 */


#include "stm32f0xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"

#include <string.h>

extern UART_HandleTypeDef huart1;

static char str[20];

void uartTask( void * pvParam )
{
	int  i = 0;

	while (2) {
		vTaskDelay( pdMS_TO_TICKS(1000) );

		snprintf(str, sizeof(str), "Hello: %u\n\n", i);
		HAL_UART_Transmit( &huart1, (uint8_t*)str, strlen(str), portMAX_DELAY );
		i++;
	}
}

