/*
 * font.c
 *
 *  Created on: Jul 4, 2018
 *      Author: rsv
 */


#include "font.h"


Symbol_t getSymImg(const Font_t *f, uint16_t code)
{
	Symbol_t sym;

	int len = f->mapSize;
	int found = 0;
	int high = len - 1;
	int low = 0;
	int middle = (high + low) / 2;
	while ( !found && high >= low )
	{
		if ( code == f->mapCI[middle].code )
		{
			found = 1;
			break;
		} else if (code < f->mapCI[middle].code ) {
			high = middle - 1;
		} else {
			low = middle + 1;
		}
		middle = (high + low) / 2;
	}
	// if not found, then return symbol '?'
	// return (found == 1) ? f->fonDes[middle].img : f_0x3F ;

	if (found) {
		sym.img   = f->mapCI[middle].img;
		sym.width = f->width;
		sym.pages = f->pages;
	} else {
		sym.img   = f->unknown;
		sym.width = f->width;
		sym.pages = f->pages;
	}

	return sym;
}
