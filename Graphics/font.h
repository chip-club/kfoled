/*
 * font.h
 *
 *  Created on: Jul 3, 2018
 *      Author: rsv
 */

#ifndef FONT_H_
#define FONT_H_

#include "stm32f0xx_hal.h"


typedef struct {
  uint16_t    code;
  const char* img;
} MapCI; // mapping about ucs16 code and symbol's image


typedef struct {
	MapCI      *mapCI;    // if null, used mapCIW ⭣
	//MapCIW     *mapCIW;   //    map to variable width font
	int         mapSize;  // count of symbols in CodeImg(W) array
	int         pages;    // height in pages (bytes) of this font
	int         width;    // default width (for mapCI)
	const char *unknown;  // symbols image if code not found
} Font_t; // info about one font with many symbols





typedef struct {
	int width;
	int pages;
	const char* img;
} Symbol_t; // info about one symbol

Symbol_t getSymImg(const Font_t *f, uint16_t code);

extern const Font_t deja17x24;
extern const Font_t font5x8;

#endif /* FONT_H_ */
