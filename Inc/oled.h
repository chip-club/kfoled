/*
 * oled.h
 *
 *  Created on: May 27, 2018
 *      Author: rsv
 */

#ifndef INC_OLED_H_
#define INC_OLED_H_

typedef enum {
	f_5x8 = 0,
	f_d24x17,
} font_e;

void disClear(void);
int  disSet( uint8_t X, uint8_t Y );
void disSetF( uint8_t X, uint8_t Y, font_e fnt );
void disPuts( const char* str );
void disPrintf( const char *format, ...);

void oledTask( void * pvParam );


#endif /* INC_OLED_H_ */
